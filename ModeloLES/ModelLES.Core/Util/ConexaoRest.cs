﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using ModelLES.Core.Aplicacao;
using RestSharp;

namespace ModelLES.Core.Util
{
    public class ConexaoRest
    {
        public string GetResponseRest(string url, string urlRequest, Method method, List<ChaveValor> parameterList, List<ChaveValor> urlSegmentList)
        { 
            var client = new RestClient(url); // baixar via nuget o RestSharp
            var request = new RestRequest(urlRequest, method);

            // Adiciona os parâmetros 
            if(parameterList != null)
                foreach (var chaveValorParameter in parameterList)
                    request.AddParameter(chaveValorParameter.Chave, chaveValorParameter.Valor);

            // Substitui o que está dentro do {} enviado no request. Exemplo "resource/{id}"
            if (urlSegmentList != null)
                foreach (var chaveValorUrlSegment in urlSegmentList)
                request.AddParameter(chaveValorUrlSegment.Chave, chaveValorUrlSegment.Valor);

            var response = client.Execute(request);
            return response.Content;
        }
    }
}