﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using ModelLES.Core.Aplicacao;
using ModelLES.Core.DAO;
using ModelLES.Core.Negocio;
using ModeloLES.Dominio;

namespace ModelLES.Core.Controle
{
    public class Fachada : IFachada
    {
        /** 
	 * Dictionarya de DAOS, serÃ¡ indexado pelo nome da entidade 
	 * O valor ï¿½ uma instÃ¢ncia do DAO para uma dada entidade; 
	 */
        private readonly IDictionary<Type, IDAO> _daos;

        /**
         * Dictionarya para conter as regras de negï¿½cio de todas operaÃ§Ãµes por entidade;
         * O valor Ã© um Dictionarya que de regras de negÃ³cio indexado pela operaÃ§Ã£o
         */
        private readonly IDictionary<Type, Dictionary<string, List<IStrategy>>> _rns;

        public Fachada()
        {
            /* Intï¿½nciando o Map de DAOS */
            _daos = new Dictionary<Type, IDAO>();
            /* Intï¿½nciando o Map de Regras de Negï¿½cio */
            _rns = new Dictionary<Type, Dictionary<string, List<IStrategy>>>();

            /* Criando instï¿½ncias dos DAOs a serem utilizados*/
            var classeExemploDAO = new ClasseExemploDAO();

            /* Adicionando cada dao no MAP indexando pelo nome da classe */
            _daos.Add(new ClasseExemplo().GetType(), classeExemploDAO);

            /* Criando instï¿½ncias de regras de negï¿½cio a serem utilizados*/
            var regra1 = new Regra1();
            var regra2 = new Regra2();
            var regra3 = new Regra3();

            /* Criando uma lista para conter as regras de negï¿½cio para quando a operaï¿½ï¿½o for obter*/
            List<IStrategy> rnsObterClasseExemplor = new List<IStrategy>();

            /* Criando uma lista para conter as regras de negï¿½cio para quando a operaï¿½ï¿½o for salvar*/
            List<IStrategy> rnsSalvarClasseExemplo = new List<IStrategy>();

            /* Criando uma lista para conter as regras de negï¿½cio para quando a operaï¿½ï¿½o for atualizar*/
            List<IStrategy> rnsAtualizarClasseExemplo = new List<IStrategy>();

            /* Criando uma lista para conter as regras de negï¿½cio para quando a operaï¿½ï¿½o for deletar*/
            List<IStrategy> rnsDeletarClasseExemplo = new List<IStrategy>();

            // Adicionando as regras de negócio para cada operação de cada objeto
            //  Obter
            rnsObterClasseExemplor.Add(regra1);
            rnsObterClasseExemplor.Add(regra2);

            //  Salvar
            rnsSalvarClasseExemplo.Add(regra2);
            rnsSalvarClasseExemplo.Add(regra3);

            //  Atualizar
            rnsAtualizarClasseExemplo.Add(regra2);
            rnsAtualizarClasseExemplo.Add(regra3);

            //  deletar
            rnsDeletarClasseExemplo.Add(regra2);
            rnsDeletarClasseExemplo.Add(regra3);

            /* Cria o mapa que poderï¿½ conter todas as listas de regras de negï¿½cio especï¿½fica 
		     * por operaï¿½ï¿½o  do fornecedor
		     */
            var rnsClasseExemplo = new Dictionary<string, List<IStrategy>>();

            /*
		     * Adiciona a listra de regras na operaï¿½ï¿½o salvar no mapa do fornecedor (lista criada na linha 70)
		     */
            rnsClasseExemplo.Add("CONSULTAR", rnsObterClasseExemplor);
            rnsClasseExemplo.Add("SALVAR", rnsSalvarClasseExemplo);
            rnsClasseExemplo.Add("ALTERAR", rnsAtualizarClasseExemplo);
            rnsClasseExemplo.Add("EXCLUIR", rnsDeletarClasseExemplo);

            /* Adiciona o mapa(criado na linha 129) com as regras indexadas pelas operaï¿½ï¿½es no mapa geral indexado 
		     * pelo nome da entidade. Observe que este mapa (rns) ï¿½ o mesmo utilizado na linha 88.
		     */
            _rns.Add(new ClasseExemplo().GetType(), rnsClasseExemplo);
        }
        private Resultado resultado;
        public Resultado Salvar(EntidadeDominio entidade)
        {
            resultado = new Resultado();
            Type nmClasse = entidade.GetType();
            string msg = executarRegras(entidade, "SALVAR");

            if (msg == null)
            {
                IDAO dao = _daos[nmClasse];
                try
                {
                    dao.Salvar(entidade);
                    List<EntidadeDominio> entidades = new List<EntidadeDominio>();
                    entidades.Add(entidade);
                    resultado.Entidades = entidades;
                }
                catch (SqlException e)
                {
                    Console.Write(e.StackTrace);
                    resultado.Msg = "NÃ£o foi possÃ­vel realizar o registro!";
                }
            }
            else
            {
                resultado.Msg = msg;
            }
            return resultado;
        }

        public Resultado Alterar(EntidadeDominio entidade)
        {
            resultado = new Resultado();
            Type nmClasse = entidade.GetType();
            string msg = executarRegras(entidade, "ALTERAR");

            if (msg == null)
            {
                IDAO dao = _daos[nmClasse];
                try
                {
                    dao.Alterar(entidade);
                    List<EntidadeDominio> entidades = new List<EntidadeDominio>();
                    entidades.Add(entidade);
                    resultado.Entidades = entidades;
                }
                catch (SqlException e)
                {
                    Console.Write(e.StackTrace);
                    resultado.Msg = "NÃ£o foi possÃ­vel realizar o registro!";
                }
            }
            else
            {
                resultado.Msg = msg;
            }
            return resultado;
        }

        public Resultado Excluir(EntidadeDominio entidade)
        {
            resultado = new Resultado();
            Type nmClasse = entidade.GetType();

            string msg = executarRegras(entidade, "EXCLUIR");

            if (msg == null)
            {
                IDAO dao = _daos[nmClasse];
                try
                {
                    dao.Excluir(entidade);
                    List<EntidadeDominio> entidades = new List<EntidadeDominio>();
                    entidades.Add(entidade);
                    resultado.Entidades = entidades;
                }
                catch (SqlException e)
                {
                    Console.Write(e.StackTrace);
                    resultado.Msg = "NÃ¢o foi possÃ­vel realizar a exlusÃ£o!";
                }
            }
            else
            {
                resultado.Msg = msg;
            }
            return resultado;
        }

        public Resultado Consultar(EntidadeDominio entidade)
        {
            resultado = new Resultado();
            Type nmClasse = entidade.GetType();

            string msg = executarRegras(entidade, "CONSULTAR");

            if (msg == null)
            {
                IDAO dao = _daos[nmClasse];
                try
                {
                    resultado.Entidades = dao.Consultar(entidade);
                }
                catch (SqlException e)
                {
                    Console.Write(e.StackTrace);
                    resultado.Msg = "NÃ¢o foi possÃ­vel realizar a consulta!";
                }
            }
            else
            {
                resultado.Msg = msg;
            }
            return resultado;
        }

        public Resultado visualizar(EntidadeDominio entidade)
        {
            resultado = new Resultado();
            resultado.Entidades = new List<EntidadeDominio>();
            resultado.Entidades.Add(entidade);
            return resultado;
        }

        private string executarRegras(EntidadeDominio entidade, string operacao)
        {
            Type nmClasse = entidade.GetType();
            StringBuilder msg = new StringBuilder();

            _rns.TryGetValue(nmClasse, out var regrasOperacao);

            if (regrasOperacao != null)
            {
                List<IStrategy> regras = regrasOperacao[operacao];

                if (regras != null)
                {
                    foreach (IStrategy s in regras)
                    {
                        string m = s.Processar(entidade);

                        if (m != null)
                        {
                            msg.Append(m);
                            msg.Append("\n");
                        }
                    }
                }
            }
            if (msg.Length > 0)
                return msg.ToString();
            else
                return null;
        }
    }
}