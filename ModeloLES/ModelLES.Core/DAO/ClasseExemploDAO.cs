﻿using System.Collections.Generic;
using ModelLES.Core.Aplicacao;
using ModelLES.Core.Util;
using ModeloLES.Dominio;
using Newtonsoft.Json;
using RestSharp;

namespace ModelLES.Core.DAO
{
    public class ClasseExemploDAO : IDAO
    {
        public void Salvar(EntidadeDominio entidade)
        {
            var url = "http://www.thales.com.br";
            var urlRequest = "api/Pessoa/";
            List<ChaveValor> parameterList = new List<ChaveValor>()
            {
                new ChaveValor()
                {
                    Chave = "Param1",
                    Valor = "FSdfdf"
                },
                new ChaveValor()
                {
                    Chave = "Param2",
                    Valor = "sdfsdf"
                }
            };
            List<ChaveValor> urlSegmentList = null;
            string json = new ConexaoRest().GetResponseRest(url, urlRequest, Method.POST, parameterList, urlSegmentList);

            // converter o json para o objeto. Instalar via NuGet o Newtonsoft.Json
            object obj = JsonConvert.DeserializeObject<object>(json);
            // verificar a resposta do rest
        }

        public void Alterar(EntidadeDominio entidade)
        {
            var url = "http://www.thales.com.br";
            var urlRequest = "api/Pessoa/";
            List<ChaveValor> parameterList = new List<ChaveValor>()
            {
                new ChaveValor()
                {
                    Chave = "Param1",
                    Valor = "FSdfdf"
                },
                new ChaveValor()
                {
                    Chave = "Param2",
                    Valor = "sdfsdf"
                }
            };
            List<ChaveValor> urlSegmentList = null;
            string json = new ConexaoRest().GetResponseRest(url, urlRequest, Method.PUT, parameterList, urlSegmentList);

            // converter o json para o objeto. Instalar via NuGet o Newtonsoft.Json
            object obj = JsonConvert.DeserializeObject<object>(json);
            // verificar a resposta do rest
        }

        public void Excluir(EntidadeDominio entidade)
        {
            var url = "http://www.thales.com.br";
            var urlRequest = "api/Pessoa/";
            List<ChaveValor> parameterList = new List<ChaveValor>()
            {
                new ChaveValor()
                {
                    Chave = "Param1",
                    Valor = "FSdfdf"
                },
                new ChaveValor()
                {
                    Chave = "Param2",
                    Valor = "sdfsdf"
                }
            };
            List<ChaveValor> urlSegmentList = null;
            string json = new ConexaoRest().GetResponseRest(url, urlRequest, Method.DELETE, parameterList, urlSegmentList);

            // converter o json para o objeto. Instalar via NuGet o Newtonsoft.Json
            object obj = JsonConvert.DeserializeObject<object>(json);
            // verificar a resposta do rest
        }

        public List<EntidadeDominio> Consultar(EntidadeDominio entidade)
        {
            var url = "http://www.thales.com.br";
            var urlRequest = "api/Pessoa/";
            List<ChaveValor> parameterList = new List<ChaveValor>()
            {
                new ChaveValor()
                {
                    Chave = "Param1",
                    Valor = "FSdfdf"
                },
                new ChaveValor()
                {
                    Chave = "Param2",
                    Valor = "sdfsdf"
                }
            };
            List<ChaveValor> urlSegmentList = null;
            string json = new ConexaoRest().GetResponseRest(url, urlRequest, Method.GET, parameterList, urlSegmentList);

            // converter o json para o objeto. Instalar via NuGet o Newtonsoft.Json
            //  So para ver funcionar.. o json está sendo criado na mão
            json = "[{\"Nome\":\"Rodrigão de LES\", \"Ativo\":true},{\"Nome\":\"Luque de ES2\", \"Ativo\":true} ]";
            List<ClasseExemplo> objList = JsonConvert.DeserializeObject<List<ClasseExemplo>>(json);

            var objReturn = new List<EntidadeDominio>();

            if(objList != null)
                foreach (var obj in objList)
                {
                    objReturn.Add(obj);
                }

            return objReturn;
        }
    }
}