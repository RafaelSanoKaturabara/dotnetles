﻿using System.Collections.Generic;
using ModeloLES.Dominio;

namespace ModelLES.Core
{
    public interface IDAO
    {
        void Salvar(EntidadeDominio entidade);
        void Alterar(EntidadeDominio entidade);
        void Excluir(EntidadeDominio entidade);
        List<EntidadeDominio> Consultar(EntidadeDominio entidade);
    }
}