﻿using ModeloLES.Dominio;

namespace ModelLES.Core.Negocio
{
    public class Regra3 : IStrategy
    {
        public string Processar(EntidadeDominio entidade)
        {
            if (entidade is ClasseExemplo)
            {
                var classeExemplo = (ClasseExemplo)entidade;

                if (classeExemplo.Ativo) // validar classe exemplo
                    return "Erro strategy regra 3";
                return null;
            }
            return "Envie um objeto classe exemplo";
        }
    }
}