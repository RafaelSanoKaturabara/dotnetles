﻿using ModeloLES.Dominio;

namespace ModelLES.Core
{
    public interface IStrategy
    {
        string Processar(EntidadeDominio entidade);
    }
}