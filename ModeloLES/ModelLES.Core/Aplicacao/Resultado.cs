﻿using System;
using System.Collections.Generic;
using ModeloLES.Dominio;

namespace ModelLES.Core.Aplicacao
{
    public class Resultado
    {
        public string Msg { get; set; }
        public List<EntidadeDominio> Entidades { get; set; }
    }
}