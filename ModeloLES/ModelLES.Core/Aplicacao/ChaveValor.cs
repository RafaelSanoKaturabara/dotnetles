﻿namespace ModelLES.Core.Aplicacao
{
    public class ChaveValor
    {
        public string Chave { get; set; }
        public string Valor { get; set; }
    }
}