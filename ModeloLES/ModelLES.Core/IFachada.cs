﻿using ModelLES.Core.Aplicacao;
using ModeloLES.Dominio;

namespace ModelLES.Core
{
    public interface IFachada
    {
        Resultado Salvar(EntidadeDominio entidade);
        Resultado Alterar(EntidadeDominio entidade);
        Resultado Excluir(EntidadeDominio entidade);
        Resultado Consultar(EntidadeDominio entidade);
    }
}