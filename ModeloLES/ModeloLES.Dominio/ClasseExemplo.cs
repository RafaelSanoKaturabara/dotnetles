﻿using System;

namespace ModeloLES.Dominio
{
    public class ClasseExemplo : EntidadeDominio
    {
        public string Nome { get; set; }
        public string DadoCriptografado { get; set; }
        public DateTime DataTempo { get; set; }
        public bool Ativo { get; set; }
        public bool? BooleanNullable { get; set; }
        public DateTime? DataTempoNullable { get; set; }
    }
}