﻿using System;

namespace ModeloLES.Dominio
{
    public class EntidadeDominio
    {
        public int Id { get; set; }
        public DateTime DtCadastro { get; set; }
    }
}