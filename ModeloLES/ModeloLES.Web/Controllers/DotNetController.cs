﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModeloLES.Dominio;

namespace ModeloLES.Web.Controllers
{
    [Produces("application/json")]
    
    public class DotNetController : Controller
    {
        // GET: /DotNet/ClasseExemploGet
        public IActionResult ClasseExemploGet(ClasseExemplo classeExemplo)
        {
            var objResult = new RodrigaoController().DoProcessRequest(classeExemplo, "CONSULTAR");
            return new ObjectResult(objResult);
        }

        [HttpPost]
        public IActionResult ClasseExemploPost(ClasseExemplo classeExemplo)
        {
            var objResult = new RodrigaoController().DoProcessRequest(classeExemplo, "SALVAR");
            return new ObjectResult(objResult);
        }

        [HttpPut]
        public IActionResult ClasseExemploPut(ClasseExemplo classeExemplo)
        {
            var objResult = new RodrigaoController().DoProcessRequest(classeExemplo, "ALTERAR");
            return new ObjectResult(objResult);
        }

        [HttpDelete]
        public IActionResult ClasseExemploDelete(ClasseExemplo classeExemplo)
        {
            var objResult = new RodrigaoController().DoProcessRequest(classeExemplo, "EXCLUIR");
            return new ObjectResult(objResult);
        }
    }
}