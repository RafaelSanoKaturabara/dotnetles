﻿using ModelLES.Core.Aplicacao;
using ModelLES.Core.Controle;
using ModeloLES.Dominio;

namespace ModeloLES.Web.Controllers.Command.Impl
{
    public class ExcluirCommand : AbstractCommand
    {
        public override Resultado Execute(EntidadeDominio entidade)
        {
            return Fachada.Excluir(entidade);
        }
    }
}