﻿using ModelLES.Core;
using ModelLES.Core.Aplicacao;
using ModelLES.Core.Controle;
using ModeloLES.Dominio;

namespace ModeloLES.Web.Controllers.Command.Impl
{
    public class AbstractCommand : ICommand
    {
        protected IFachada Fachada = new Fachada();
        public virtual Resultado Execute(EntidadeDominio entidade)
        {
            throw new System.NotImplementedException(); // Este método vai sofrer polimorfismo. Obs: No java não precisa implementar este método
        }
    }
}