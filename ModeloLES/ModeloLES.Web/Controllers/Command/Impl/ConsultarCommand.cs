﻿using ModelLES.Core.Aplicacao;
using ModeloLES.Dominio;

namespace ModeloLES.Web.Controllers.Command.Impl
{
    public class ConsultarCommand : AbstractCommand
    {
        public override Resultado Execute(EntidadeDominio entidade)
        {
            return Fachada.Consultar(entidade);
        }
    }
}