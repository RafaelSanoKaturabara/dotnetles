﻿using ModelLES.Core.Aplicacao;
using ModeloLES.Dominio;

namespace ModeloLES.Web.Controllers.Command.Impl
{
    public class AlterarCommand : AbstractCommand
    {
        public override Resultado Execute(EntidadeDominio entidade)
        {
            return Fachada.Alterar(entidade);
        }
    }
}