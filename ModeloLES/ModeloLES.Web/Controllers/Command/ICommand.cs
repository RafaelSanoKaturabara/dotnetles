﻿using ModelLES.Core.Aplicacao;
using ModeloLES.Dominio;

namespace ModeloLES.Web.Controllers.Command
{
    public interface ICommand
    {
        Resultado Execute(EntidadeDominio entidade);
    }
}