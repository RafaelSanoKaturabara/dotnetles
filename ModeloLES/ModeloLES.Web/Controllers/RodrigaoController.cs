﻿using System;
using System.Collections.Generic;
using ModelLES.Core.Aplicacao;
using ModeloLES.Dominio;
using ModeloLES.Web.Controllers.Command;
using ModeloLES.Web.Controllers.Command.Impl;

namespace ModeloLES.Web.Controllers
{
    public class RodrigaoController
    {
        private static Dictionary<string, ICommand> _commands;
        //private static Dictionary<string, IViewHelper> vhs;

        public RodrigaoController()
        {
            /* Utilizando o command para chamar a fachada e indexando cada command 
    	 * pela operaï¿½ï¿½o garantimos que esta servelt atenderï¿½ qualquer operaï¿½ï¿½o */
            _commands = new Dictionary<string, ICommand>();
            _commands.Add("SALVAR", new SalvarCommand());
            _commands.Add("EXCLUIR", new ExcluirCommand());
            _commands.Add("CONSULTAR", new ConsultarCommand());
            _commands.Add("ALTERAR", new AlterarCommand());
        }

        public Resultado DoProcessRequest(EntidadeDominio entidade, string strOperacao)
        {
            
                //ObtÃ©m a uri que invocou esta servlet (O que foi definido no methdo do form html)

                //string uri = request.getRequestURI();

                ////ObtÃ©m a operaÃ§Ã£o executada
                //string operacao = request.getParameter("operacao");

                //ObtÃ©m um viewhelper indexado pela uri que invocou esta servlet
                //IViewHelper vh = vhs.get(uri);

                //O viewhelper retorna a entidade especifica para a tela que chamou esta servlet
                //EntidadeDominio entidade = vh.getEntidade(request);

                //ObtÃ©m o command para executar a respectiva operaÃ§Ã£o
            ICommand command = _commands[strOperacao];

                /*Executa o command que chamara a fachada para executar a opeÃ§Ã£o requisitada
                 * o retorno Ã© uma instÃ¢ncia da classe resultado que pode conter mensagens derro 
                 * ou entidades de retorno
                 */
            Resultado resultado = command.Execute(entidade);

            return resultado;

                //// Salvando log
                //saveLog(entidade, resultado, request);

                /*
                 * Executa o mï¿½todo setView do view helper especÃ­fico para definir como deverï¿½ ser apresentado 
                 * o resultado para o usuÃ¡rio
                 */
               // vh.setView(resultado, request, response);
        }
    }
}